#!/usr/bin/env python3

import os, sys, argparse, logging, json, time
from bottle import route, request, response, default_app, view, static_file, template
from envirophat import light, weather, motion, leds

def get_data():
	rgb = light.rgb()
	data = {
		"temp": round(weather.temperature()),
		"pressure": round(weather.pressure()),
		"light": round(light.light()),
		"heading": motion.heading(), 
		"color": "{},{},{}".format(rgb[0], rgb[1], rgb[2])
	}
	return data

@route('/static/<filepath:path>')
def server_static(filepath):
	return static_file(filepath, root='views/static')

@route('/')
def index():
	return template('index', timeNow=time.strftime("%H:%M", time.gmtime()), data=get_data())

@route("/data.json")
def json_data():
	data = get_data()
	response.content_type = 'application/json'
	return json.dumps(data)

if __name__ == '__main__':

	parser = argparse.ArgumentParser()

	# Save to file
	parser.add_argument("-i", "--host", default=os.getenv('IP', '127.0.0.1'), help="IP Address")
	parser.add_argument("-p", "--port", default=os.getenv('PORT', 5000), help="Port")

	# Verbose mode
	parser.add_argument("--verbose", "-v", help="increase output verbosity", action="store_true")
	args = parser.parse_args()

	if args.verbose:
		logging.basicConfig(level=logging.DEBUG)
	else:
		logging.basicConfig(level=logging.INFO)
	log = logging.getLogger(__name__)

	try:
		# For color sensing, we're going to turn the LEDs on
		leds.on()
		app = default_app()
		app.run(host=args.host, port=args.port, server='tornado')
	except:
		log.error("Unable to start server on {}:{}".format(args.host, args.port))
	finally:
		# Once we're finished, turn everything off
		leds.off()