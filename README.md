# berrycast

berrycast makes using the pimoroni [enviroPHAT](https://shop.pimoroni.com/products/enviro-phat) into a useful weather station!

## Kit
 - [Raspberry Pi](https://thepihut.com/collections/raspberry-pi/products/raspberry-pi-3-model-b)
 - [Enviro pHAT](https://shop.pimoroni.com/products/enviro-phat)
 - Wi-Fi connection (Built into the Raspberry Pi 3), or Ethernet connection

## Installation

Simply run:

    $ git clone https://codedin.wales/picymru/berrycast.git berrycast
    $ cd berrycast
    $ sudo pip install -r requirements.txt


## Usage

### To start the server, listening only on the device (no external network access)

    $ python3 application.py -i 127.0.0.1

### To start the server, listening on all network interfaces (not recommended for internet facing)

    $ python3 application.py -i 0.0.0.0

### For more help

	$ python3 application.py --help
	usage: application.py [-h] [-i HOST] [-p PORT] [--verbose]

	optional arguments:
	  -h, --help            show this help message and exit
	  -i HOST, --host HOST  IP Address
	  -p PORT, --port PORT  Port
	  --verbose, -v         increase output verbosity

## License

This project has been developed by PiCymru as part of our education and learning programme. This project is hereby released under the terms of the MIT License, and is included below

	MIT License

	Copyright (c) 2017 PiCymru

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

## Support

Have a question? Need assistance? Don't stay stuck! If you do use this project or have any feedback we would love to hear from you, tweet us at [@PiCymru](https://twitter.com/PiCymru) or drop us an [e-mail](mailto:hello@picymru.org.uk)