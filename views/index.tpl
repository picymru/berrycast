<!doctype html>
<head>
	<title>berrycast</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="refresh" content="5">
	
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/static/css/ionicons.min.css">
	<link rel="stylesheet" href="/static/css/style.css" />
</head>
<body>
	<div class="wrapper">
		<div class="flex">
			<div class="info">
				<h2 class="info__title">berrycast</h2>
				<div class="info__time">{{timeNow}}</div>
			</div>
			<div class="menu">
				<ul class="menu__list">
					<li class="menu__item menu__item--active">Live</li>			
				</ul>
				<div class="flex">
					<div class="menu-border  one-half"></div>
				</div>
			</div>
		</div>
		
		<div class="statistics">
			<div class="consumption-panel">
				<div class="flex flex--justify-between">
					<p>
						Temperature				
					</p>
				</div>
				<h1 class="consumption">{{data['temp']}}&deg;c</h1>
			</div>
			
			<div class="flex-1">
				<ul class="lamps__list">
					<li class="lamps__item  lamps__total">
						<h2 class="lamps__count">{{data['pressure']}} pa</h2>
						<p class="lamps__description">Pressure</p>
					</li>
					
					<li class="lamps__item  lamps__active">
						<h2 class="lamps__count">{{data['light']}} lux</h2>
						<p class="lamps__description">Light</p>
					</li>
				</ul>
			</div> <!-- /.right-column -->
		</div> <!-- /.statistics -->
	</div>
</body>
</html>